from django import forms
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from django.shortcuts import render, get_object_or_404, redirect

from admin_site.choices import category
from admin_site.models import User


class UpdateProfileForm(forms.Form):
    username=forms.CharField(max_length=150)
    email=forms.EmailField()
    first_name=forms.CharField(max_length=30)
    last_name=forms.CharField(max_length=30)
    address=forms.CharField(max_length=500)
    # business_domain = forms.ChoiceField(choices=category, widget=forms.Select())
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_no = forms.CharField(max_length=15, validators=[phone_regex])
    zip_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    zip_code = forms.IntegerField(max_value=9999, min_value=1000)



    # def clean(self):
    #     cleaned_data = super(ProfileForm, self).clean()
    #     password = cleaned_data.get("password")
    #     confirm_password = cleaned_data.get("confirm_password")
    #
    #     if password != confirm_password:
    #         raise forms.ValidationError(
    #             "Password and confirm password does not match"
    #         )
# def UpdateProfile(request):


def profile(request):
    user=get_object_or_404(User,id=request.user.id)
    data={}
    data['username']=user.username
    data['email']=user.email
    data['first_name']=user.first_name
    data['last_name']=user.last_name
    data['address']=user.client.address
    # data['business_domain']=user.client.business_domain
    data['phone_no']=user.client.phone
    data['zip_code']=user.client.zip_code


    form=UpdateProfileForm(data)
    if request.method == 'POST':
        form=UpdateProfileForm(request.POST or None)

        if form.is_valid():
            user.username=form.cleaned_data['username']
            user.email=form.cleaned_data['email']
            user.first_name=form.cleaned_data['first_name']
            user.last_name=form.cleaned_data['last_name']
            user.client.phone=form.cleaned_data['address']
            user.client.zip_code=form.cleaned_data['phone_no']
            user.save()

            return redirect('admin_site:profile')

    return render(request,"admin_site/profile/profile-details.html",{'form':form})

# def update_profile(request):
