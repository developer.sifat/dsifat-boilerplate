from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.sites.models import Site
from django.urls import reverse
from admin_site.models import Client, SiteCredentials


class MyAccountAdapter(DefaultAccountAdapter):

    def save_user(self, request, user, form, commit=True):
        data=form.cleaned_data
        user.username=data.get('username')
        user.email=data.get('email')
        user.is_client=True
        if 'password1' in data:
            user.set_password(data['password1'])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            user.save()
        client=Client.objects.create(user=user)
        client.phone=data.get('phone_no')
        client.address=data.get('address')
        client.zip_code=data.get('zip_code')
        client.business_domain=data.get('business_domain')
        client.save()
        return user

    def get_login_redirect_url(self, request):
        if request.user.is_superuser == True:
            return reverse('admin_site:dashboard')
        elif request.user:
            return reverse('admin_site:dashboard')
        else:
            return reverse('admin_site:dashboard')
