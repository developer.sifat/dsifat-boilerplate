# def review_list(request):
#     queryset=Review.objects.all()
#     page = request.GET.get('page')
#
#     paginator = Paginator(queryset, 10)
#
#     try:
#         queryset = paginator.page(page)
#
#     except PageNotAnInteger:
#         queryset = paginator.page(1)
#
#     except EmptyPage:
#         queryset = paginator.page(paginator.num_pages)
#
#     return render(request,"admin_site/reviews/review_list.html",{'objects':queryset})
#
# # @login_required
# def review_create(request):
#     if request.method=='POST':
#         form = ReviewForm(request.POST or None)
#         if form.is_valid():
#             form.save()
#             return redirect("admin_site:review_list")
#     else:
#         form = ReviewForm()
#
#     return render(request,"admin_site/reviews/review_create.html",{'form':form})
#
# @login_required
# def review_edit(request,id):
#     review=get_object_or_404(Review,id=id)
#     form=ReviewForm(request.POST or None, instance=review)
#
#     if form.is_valid():
#         form.save()
#         return redirect('admin_site:review_list')
#
#     return render(request,"admin_site/reviews/review_update.html",{'form':form})
#
# @login_required
# def review_delete(request,id):
#     review=get_object_or_404(Review,id=id)
#     review.delete()
#     return redirect("admin_site:review_list")
#
