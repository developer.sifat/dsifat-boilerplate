from __future__ import absolute_import, print_function, unicode_literals

from django.conf.urls import url, include
from django.contrib import admin

from mainsite.views import homepage, redirection, payment, subscribe_redirect, payment_stripe, portfolio_repair_shop

admin.autodiscover()


urlpatterns = [
    url(r'^$',view=homepage,name="homepage"),
    url(r'^redirection/$',view=redirection,name="redirection"),
    url(r'^payment/$',view=payment,name="payment"),
    url(r'^subscribe-redirect/$',view=subscribe_redirect,name="subscribe-redirect"),
    url(r'^payment-stripe/$',view=payment_stripe,name="payment_stripe"),

    url(r'^category/repair-shop/$', view=portfolio_repair_shop, name="portfolio_repair_shop"),
]
